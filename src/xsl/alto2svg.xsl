<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:svg="http://www.w3.org/2000/svg"
    xmlns:alto="http://www.loc.gov/standards/alto/ns-v4#"
    xpath-default-namespace="http://www.loc.gov/standards/alto/ns-v4#"
    xmlns="http://www.w3.org/2000/svg"
    exclude-result-prefixes="xs alto"
    version="2.0">
    <xsl:param name="folder">BnF-837</xsl:param>
    <xsl:template match="/">
        
        <!--<xsl:result-document href="test.svg">-->
        <svg  xmlns="http://www.w3.org/2000/svg"     width="{//alto:Page[1]/@WIDTH}" height="{//alto:Page[1]/@HEIGHT}">
          
            <image>
                <xsl:attribute name="href"><xsl:value-of select="concat('https://gitlab.huma-num.fr/jnuguet/test-3/-/raw/main/src/data/',$folder,'/',//alto:fileName[1]/text())"/></xsl:attribute>
                <xsl:attribute name="width"><xsl:value-of select="//alto:Page[1]/@WIDTH"/></xsl:attribute>
                <xsl:attribute name="height"><xsl:value-of select="//alto:Page[1]/@HEIGHT"/></xsl:attribute>
                <xsl:attribute name="opacity">0.5</xsl:attribute>
            </image>
                <xsl:apply-templates select="//Layout"/>
        </svg>
      <!--  </xsl:result-document>-->
    </xsl:template>
    <xsl:template match="alto:TextBlock">
        
        
        <rect stroke="blue" fill="transparent">
            <xsl:attribute name="x"><xsl:value-of select="number(@HPOS)"/></xsl:attribute>
            <xsl:attribute name="y"><xsl:value-of select="number(@VPOS)"/></xsl:attribute>
            <xsl:attribute name="width"><xsl:value-of select="number(@WIDTH)"/></xsl:attribute>
            <xsl:attribute name="height"><xsl:value-of select="number(@HEIGHT)"/></xsl:attribute>
        </rect>
        <text fill="blue">
            <xsl:attribute name="x"><xsl:value-of select="number(@HPOS)"/></xsl:attribute>
            <xsl:attribute name="y"><xsl:value-of select="number(@VPOS)"/></xsl:attribute>
            <xsl:value-of select="@ID"/>
        </text>
        <g>
            <xsl:apply-templates/>
        </g>
        
        
    </xsl:template>
    
    <xsl:template match="alto:TextLine">
        
      
        <rect stroke="purple" fill="transparent">
            <xsl:attribute name="x"><xsl:value-of select="number(@HPOS)"/></xsl:attribute>
            <xsl:attribute name="y"><xsl:value-of select="number(@VPOS)"/></xsl:attribute>
            <xsl:attribute name="width"><xsl:value-of select="number(@WIDTH)"/></xsl:attribute>
            <xsl:attribute name="height"><xsl:value-of select="number(@HEIGHT)"/></xsl:attribute>
        </rect>
        <text fill="red">
            <xsl:attribute name="x"><xsl:value-of select="number(@HPOS)"/></xsl:attribute>
            <xsl:attribute name="y"><xsl:value-of select="number(@VPOS)+ (number(@HEIGHT) div 2)"/></xsl:attribute>
            <xsl:value-of select="@ID"/></text>
        <g>
            <xsl:apply-templates select="alto:Shape[1]"/>
        </g>
        

    </xsl:template>
    
    <xsl:template match="alto:Polygon ">
        <xsl:variable name="points" select="tokenize(@POINTS,' ')"/>
        <polygon>
            <xsl:attribute name="fill">transparent</xsl:attribute>
            <xsl:attribute name="stroke">green</xsl:attribute>
            <xsl:attribute name="points">
        <xsl:for-each select="$points">
          <!--  <xsl:comment> <xsl:value-of select="."/></xsl:comment>-->
            <xsl:choose>
 <xsl:when test="position() mod 2">
                <xsl:value-of select="."/><xsl:text> </xsl:text>
            </xsl:when>
                <xsl:otherwise><xsl:value-of select="."/>,</xsl:otherwise>
            </xsl:choose>
           
       
        </xsl:for-each> 
            </xsl:attribute>
     </polygon>
       
        
    </xsl:template>
</xsl:stylesheet>